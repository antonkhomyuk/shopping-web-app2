import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http'

@Injectable()
export class ServerService {
    constructor(private http: HttpClient){

    }
    storeServers(servers: any[]) {
      return this.http.post('https://shopping-backend-firebas-f5a89-default-rtdb.firebaseio.com/', servers);
    }
}
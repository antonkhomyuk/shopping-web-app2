import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  title = 'shopping web app';
  user = 'user@gmail.com';

  constructor() { }

  ngOnInit(): void {
  }

}

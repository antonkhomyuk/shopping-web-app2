// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyD-IquPdialhvhma6MM_ld_tjCdjx_UPqI",
    authDomain: "shopping-backend-firebas-f5a89.firebaseapp.com",
    projectId: "shopping-backend-firebas-f5a89",
    storageBucket: "shopping-backend-firebas-f5a89.appspot.com",
    messagingSenderId: "256513061195",
    appId: "1:256513061195:web:f16ac08da80e6aa53bf8ce",
    measurementId: "G-CRP1JR8Q6D"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
